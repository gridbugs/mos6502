use ines::Ines;
use mos6502_assembler::{Addr, Block, LabelRelativeOffset};
use mos6502_model::{interrupt_vector, Address};

const PRG_START: Address = 0x8000;
const INTERRUPT_VECTOR_START_PC_OFFSET: Address = interrupt_vector::START_LO - PRG_START;
const INTERRUPT_VECTOR_NMI_OFFSET: Address = interrupt_vector::NMI_LO - PRG_START;

fn program(b: &mut Block) {
    use mos6502_model::addressing_mode::*;
    use mos6502_model::assembler_instruction::*;

    let write_literal_byte = |b: &mut Block, addr: u16, value: u8| {
        b.inst(Lda(Immediate), value);
        b.inst(Sta(Absolute), Addr(addr));
    };

    let write_literal_byte_zero_page = |b: &mut Block, addr: u8, value: u8| {
        b.inst(Lda(Immediate), value);
        b.inst(Sta(ZeroPage), addr);
    };

    // Add the interrupt vector at the end of the ROM
    b.set_offset(INTERRUPT_VECTOR_START_PC_OFFSET);
    b.label_offset_le("reset");
    b.set_offset(INTERRUPT_VECTOR_NMI_OFFSET);
    b.label_offset_le("nmi");

    // Start adding instructions to PRG ROM at the beginning of memory
    b.set_offset(0);

    // Simple interrupt handler for non-maskable interrupts
    b.label("nmi");

    // This code runs once per frame

    mod debug {
        pub const MUSIC_COUNTER_SPRITE_Y: u8 = 16;
    }

    mod timing {
        pub const FRAMES_PER_SEMIQUAVER: u8 = 6;
        pub const FRAMES_PER_BEAT: u8 = FRAMES_PER_SEMIQUAVER * 4;
    }

    mod variables {
        pub const COUNTDOWN_SEMIQUAVER: u8 = 0;
        pub const COUNTDOWN_BEAT: u8 = 1;
    }

    // decrement the semiquaver countdown
    b.inst(Dec(ZeroPage), variables::COUNTDOWN_SEMIQUAVER);
    b.inst(Bne, LabelRelativeOffset("end_countdown_semiquaver_zero"));
    write_literal_byte_zero_page(
        b,
        variables::COUNTDOWN_SEMIQUAVER,
        timing::FRAMES_PER_SEMIQUAVER,
    );
    b.label("end_countdown_semiquaver_zero");

    // decrement the semiquaver countdown
    b.inst(Dec(ZeroPage), variables::COUNTDOWN_BEAT);
    b.inst(Bne, LabelRelativeOffset("end_beat_countdown_zero"));
    write_literal_byte_zero_page(b, variables::COUNTDOWN_BEAT, timing::FRAMES_PER_BEAT);
    b.label("end_beat_countdown_zero");

    // draw a debugging sprite based on the music counter
    write_literal_byte(b, 0x0200, debug::MUSIC_COUNTER_SPRITE_Y);
    write_literal_byte(b, 0x0201, 1);
    write_literal_byte(b, 0x0202, 0);
    b.inst(Lda(ZeroPage), variables::COUNTDOWN_BEAT);
    b.inst(Asl(Accumulator), ());
    b.inst(Asl(Accumulator), ());
    b.inst(Sta(Absolute), Addr(0x203));

    // perform sprite oam dma
    write_literal_byte(b, 0x4014, 0x02);

    b.inst(Rti, ());

    // Execution will start at the "reset" label. This is our program's entry point.
    b.label("reset");

    // Intialize variables
    write_literal_byte_zero_page(
        b,
        variables::COUNTDOWN_SEMIQUAVER,
        timing::FRAMES_PER_SEMIQUAVER,
    );
    write_literal_byte_zero_page(b, variables::COUNTDOWN_BEAT, timing::FRAMES_PER_BEAT);

    b.inst(Lda(Immediate), 0b00000000);
    b.inst(Sta(Absolute), Addr(0x2000));

    b.inst(Lda(Immediate), 0b00011110);
    b.inst(Sta(Absolute), Addr(0x2001));

    // Read PPU status register to clear address latch
    b.inst(Bit(Absolute), Addr(0x2002));

    // Write palette address (0x3F00) to PPU address register
    b.inst(Lda(Immediate), 0x3F);
    b.inst(Sta(Absolute), Addr(0x2006));
    b.inst(Lda(Immediate), 0x00);
    b.inst(Sta(Absolute), Addr(0x2006));

    #[rustfmt::skip]
    let palette = [
        0x11, // dark blue (background)
        0x24, // pink
        0x14, // dark pink
        0x3d, // medium grey
    ];

    // write universal background colour and first background palette
    for &c in &palette {
        b.inst(Lda(Immediate), c);
        b.inst(Sta(Absolute), Addr(0x2007));
    }

    // Write address of start of sprite palette (0x3F11) to PPU address register
    b.inst(Lda(Immediate), 0x3F);
    b.inst(Sta(Absolute), Addr(0x2006));
    b.inst(Lda(Immediate), 0x11);
    b.inst(Sta(Absolute), Addr(0x2006));

    // write first sprite palette, skipping over the first entry which is the universal background
    for &c in &palette[1..] {
        b.inst(Lda(Immediate), c);
        b.inst(Sta(Absolute), Addr(0x2007));
    }

    // enable vblank nmi
    b.inst(Lda(Immediate), 0b10000000);
    b.inst(Sta(Absolute), Addr(0x2000));

    b.label("spin");
    b.inst(Jmp(Absolute), "spin");
}

fn prg_rom() -> Vec<u8> {
    let mut block = Block::new();
    program(&mut block);
    let mut prg_rom = Vec::new();
    block
        .assemble(PRG_START, ines::PRG_ROM_BLOCK_BYTES * 2, &mut prg_rom)
        .expect("Failed to assemble");
    prg_rom
}

#[rustfmt::skip]
fn chr_rom() -> Vec<u8> {
    vec![
        // 0
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,

        // 1
        0b00000000,
        0b01111000,
        0b01111000,
        0b01111000,
        0b01111000,
        0b00000000,
        0b00000000,
        0b00000000,

        0b00000000,
        0b00000000,
        0b00000000,
        0b00011110,
        0b00011110,
        0b00011110,
        0b00011110,
        0b00000000,
    ]
}

fn main() {
    use std::io::Write;
    let ines = Ines {
        header: ines::Header {
            num_prg_rom_blocks: 2,
            num_chr_rom_blocks: 1,
            mapper: ines::Mapper::Nrom,
            mirroring: ines::Mirroring::Vertical,
            four_screen_vram: false,
        },
        prg_rom: prg_rom(),
        chr_rom: chr_rom(),
    };
    let mut encoded = Vec::new();
    ines.encode(&mut encoded);
    std::io::stdout()
        .lock()
        .write_all(&encoded)
        .expect("Failed to write encoded rom to stdout");
}
